<?php
include_once '../../vendor/autoload.php';
include_once '../include/explore/header.php';
?>

<body id="home">

<!-- ****************************** Preloader ************************** -->
<div id="preloader"></div>


<!--Fixed Navigation-->

<header id="navigation" class="navbar-fixed-top navbar">
    <div class="container">

        <div class="navbar-header">
            <!-- responsive nav button -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars fa-2x"></i>
            </button>
            <!-- /responsive nav button -->

            <!-- logo -->
            <a class="navbar-brand hvr-push" href="#home">
                <h1 id="logo">
                    <img src="assets/exploring/header/img/logo.png">
                </h1>
            </a>
            <!-- /logo -->
        </div>

        <!-- main nav -->
        <nav class="collapse navbar-collapse navbar-right" role="navigation">
            <ul id="nav" class="nav navbar-nav">
                <li class="current hvr-box-shadow-outset">
                    <a href="#home">Home</a>
                </li>
                <li class="hvr-box-shadow-outset"><a href="#video">Video</a></li>
                <li class="hvr-box-shadow-outset"><a href="#features">Features</a></li>
                <li class="hvr-box-shadow-outset"><a href="#works">Work</a></li>
                <!--                {{--<li class="hvr-box-shadow-outset"><a href="#team">Team</a></li>--}}-->
                <li class="hvr-box-shadow-outset"><a href="#contact">Contact</a></li>
            </ul>
        </nav>
        <!-- /main nav -->

    </div>
</header>

<!--End Fixed Navigation-->

<!--Home Slider-->

<section id="slider" style="height: 100vh">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Indicators bullet -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>
        <!-- End Indicators bullet -->

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <!-- single slide -->
            <div class="item active" style="background-image: url(assets/exploring/header/img/banner-1.jpg)">
                <div class="carousel-caption">
                    <h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated" style=" color: white; font-family: 'Acme', sans-serif">Meet<span style="font-weight: normal"> Monir</span> !</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated" style="font-family: 'Acme', sans-serif"><span class="color">/creative</span> <span style="color: whitesmoke">web design & development</span></h3>
                    <p data-wow-duration="1000ms" class="wow slideInRight animated">We are a team of professionals</p>

                    <ul class="social-links text-center">
                        <li><a href="https://goo.gl/Wffie7" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
                        <li><a href="https://goo.gl/KLo5NS" target="_blank"><i class="fa fa-facebook fa-lg"></i></a></li>
                        <li><a href="https://goo.gl/p5vR1a" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a></li>
                        <li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end single slide -->

            <!-- single slide -->
            <div class="item" style="background-image: url(assets/exploring/header/img/banner-2.jpg)">
                <div class="carousel-caption">
                    <h2 data-wow-duration="500ms" data-wow-delay="500ms" class="wow bounceInDown animated" style="font-family: 'Acme', sans-serif; color: white">Meet<span style="font-weight: normal"> Monir</span>!</h2>
                    <h3 data-wow-duration="1000ms" class="wow slideInLeft animated" style="font-family: 'Acme', sans-serif"><span class="color">/creative</span> <span style="color: whitesmoke">web design & development</span></h3>
                    <p data-wow-duration="1000ms" class="wow slideInRight animated">We are a team of professionals</p>

                    <ul class="social-links text-center">
                        <li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
                        <li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- end single slide -->

        </div>
        <!-- End Wrapper for slides -->

    </div>
</section>

<!--End Home SliderEnd-->


<!-- ****************************** Video section ************************** -->
<section id="video" class="block">
    <div class="container">

        <div class="title-box">
            <h1 class="block-title wow animated zoomIn">
                <span class="bb-top-left"></span>
                <span class="bb-bottom-left"></span>
                Video Preview
                <span class="bb-top-right"></span>
                <span class="bb-bottom-right"></span>
            </h1>
        </div>

        <div class="row">
            <div class="col-sm-offset-2 col-sm-8 wow animated bounceInUp">
                <!-- 16:9 aspect ratio -->
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Q_ygOSGmsK4?rel=0&amp;showinfo=0" frameborder="2"></iframe>
                </div>
                <p class="text-center italic">
                    <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</strong>
                </p>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Specialty section ************************** -->
<section id="features" class="block">
    <div class="container">
        <div class="title-box">
            <h1 class="block-title wow animated zoomIn">
                <span class="bb-top-left"></span>
                <span class="bb-bottom-left"></span>
                Specialty
                <span class="bb-top-right"></span>
                <span class="bb-bottom-right"></span>
            </h1>
        </div>

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <i class="fa fa-sliders"></i>
                    <h2>Tune up</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <i class="fa fa-unlock-alt"></i>
                    <h2>Security</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <div style="margin-left: 140px; margin-top: -40px; margin-bottom: 10px">
                        <img src="assets/exploring/img/icon/radiation.png" alt="" class="img-responsive" style="height: 70px">
                    </div>
                    <h2>Firewall</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <i class="fa fa-eercast"></i>
                    <h2>HD Widgets</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <i class="fa fa-search"></i>
                    <h2>Data Sync</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="feature-box wow animated flipInX animated">
                    <i class="fa fa-refresh"></i>
                    <h2>Customize</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>


<!--Our Works-->

<section id="works" class="works clearfix">
    <div class="container">
        <div class="row">

            <div class="title-box">
                <h1 class="block-title wow animated zoomIn">
                    <span class="bb-top-left"></span>
                    <span class="bb-bottom-left"></span>
                    Works
                    <span class="bb-top-right"></span>
                    <span class="bb-bottom-right"></span>
                </h1>
            </div>

            <div class="sec-sub-title text-center">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore</p>
            </div>

            <div class="work-filter wow fadeInRight animated" data-wow-duration="500ms">
                <ul class="text-center">
                    <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
                    <li><a href="javascript:;" data-filter=".laravel" class="filter">Laravel</a></li>
                    <li><a href="javascript:;" data-filter=".psd_html" class="filter">PSD to HTML</a></li>
                    <li><a href="javascript:;" data-filter=".bootstrap" class="filter">Bootstrap</a></li>
                    <li><a href="javascript:;" data-filter=".photography" class="filter">photography</a></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="project-wrapper">

        <figure class="mix work-item laravel bootstrap">
            <img src="assets/exploring/header/img/works/thumbnails/item-1.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-1.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Photography</p>
            </figcaption>
        </figure>

        <figure class="mix work-item psd_html bootstrap">
            <img src="assets/exploring/header/img/works/thumbnails/item-2.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-2.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item bootstrap psd_html">
            <img src="assets/exploring/header/img/works/thumbnails/item-3.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="http://paper-planes.netlify.com/" target="_blank"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item photography">
            <img src="assets/exploring/header/img/works/thumbnails/item-4.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-4.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item laravel bootstrap">
            <img src="assets/exploring/header/img/works/thumbnails/item-5.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-5.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item psd_html bootstrap">
            <img src="assets/exploring/header/img/works/thumbnails/item-6.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-6.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item bootstrap">
            <img src="assets/exploring/header/img/works/thumbnails/item-7.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-7.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

        <figure class="mix work-item photography">
            <img src="assets/exploring/header/img/works/thumbnails/item-8.jpg" alt="">
            <figcaption class="overlay">
                <a class="fancybox" rel="works" title="Live Preview" href="assets/exploring/header/img/works/preview/item-8.jpg"><i class="fa fa-eye fa-lg"></i></a>
                <h4>Labore et dolore magnam</h4>
                <p>Laravel</p>
            </figcaption>
        </figure>

    </div>

</section>

<!--End Our Works-->

<!-- ****************************** Pricing section ************************** -->
<section id="pricing" class="block">
    <div class="banner-overlay bg-color-grad"></div>
    <div class="container">
        <div class="title-box">
            <h1 class="block-title title-light wow animated zoomIn">
                <span class="bb-top-left"></span>
                <span class="bb-bottom-left"></span>
                Affordable Packages
                <span class="bb-top-right"></span>
                <span class="bb-bottom-right"></span>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <ul class="pricing-table">
                    <li class="wow flipInY animated" style="visibility: visible;">
                        <h3>Standard</h3>
<!--                        <span> $2.99 <small>per month</small> </span>-->
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li class="not">Multiplatform</li>
                            <li class="not">Video background</li>
                            <li class="not">Support</li>
                        </ul>
                        <a href="mailto:mhossain143094@bscse.uiu.ac.bd" target="_top" class="buy"><div class="hvr-buzz"><i class="fa fa-envelope" aria-hidden="true"></i></div></a>
                    </li>
                    <li class="gold wow flipInY animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                        <div class="stamp"><i class="fa fa-star-o" aria-hidden="true"></i>Best choice</div>
                        <h3>Gold</h3>
<!--                        <span> $7.99 <small>per month</small> </span>-->
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li>Multiplatform</li>
                            <li>Video background</li>
                            <li>Support</li>
                        </ul>
                        <a href="mailto:mhossain143094@bscse.uiu.ac.bd" target="_top" class="buy"><div class="hvr-buzz"><i class="fa fa-envelope" aria-hidden="true"></i></div></a>
                    </li>
                    <li class="silver wow flipInY animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                        <h3>Sliver</h3>
<!--                        <span> $4.99 <small>per month</small> </span>-->
                        <ul class="benefits-list">
                            <li>Responsive</li>
                            <li>Documentation</li>
                            <li>Multiplatform</li>
                            <li class="not">Video background</li>
                            <li class="not">Support</li>
                        </ul>
                        <a href="mailto:mhossain143094@bscse.uiu.ac.bd" target="_top" class="buy"><div class="hvr-buzz"><i class="fa fa-envelope" aria-hidden="true"></i></div></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Subscribe section ************************** -->
<section id="subscribe" class="block">
    <div class="banner-overlay" style="background: rgba(255, 255, 255, 0.8)"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <p class="susbcribe-text">
                    <strong>Subscribe to our email list to get the latest update about our amazing app</strong>
                    <br/>Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat.</p>
            </div>
        </div>
    </div>
    <div class="container subscribe-wrap">
        <div class="row">
            <div class="col-sm-12 wow animated bounceInUp">
                <form role="form" class="mailchimp">

                    <div class="input-group">
                        <input type="email" name="email" id="subscriber-email" class="form-control" placeholder="Your Email Address">
                        <span class="input-group-btn">
						    <button class="btn btn-custom" type="submit" id="subscribe-button">GET UPDATES</button>
						  </span>
                    </div><!-- /input-group -->

                    <!-- SUCCESS OR ERROR MESSAGES -->
                    <span class="subscription-success"></span>
                    <span class="subscription-error"></span>

                </form>
            </div>
        </div>
    </div>
</section>


<!-- ****************************** Contact section ************************** -->
<section id="contact" class="block">
    <div class="container">
        <div class="title-box">
            <h1 class="block-title wow animated zoomIn">
                <span class="bb-top-left"></span>
                <span class="bb-bottom-left"></span>
                Contact Us
                <span class="bb-top-right"></span>
                <span class="bb-bottom-right"></span>
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-4 address wow fadeInLeft animated">
                <ul class="address-list">
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> <span>6/3A, Orphanges Road <br>Lalbag, Dhaka - 1211</span></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> <span>(880)-168-3852-548 </span></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i> <span>mhshujon@gmail.com</span></li>
                    <li><i class="fa fa-globe" aria-hidden="true"></i> <span>www.mhshujon.me</span></li>
                </ul>
            </div><!-- address -->
            <div class="col-sm-8 mailbox wow fadeInRight animated">
                <form name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Name *" id="name" required>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Your Subject *" id="subject" required data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                                <div id="success"></div>
                                <button type="submit" class="polo-btn contact-submit"><i class="fa fa-paper-plane-o"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-danger error">Error!  E-mail must be valid and message must be longer than 1 character.</div>
                    <div class="alert alert-success success">Your message has been sent successfully.</div>
                </form>
            </div><!--/send mail-->
        </div><!--/row-->
    </div>
    <div class="clearfix"></div>
</section>


<!-- ****************************** Footer ************************** -->
<section id="footer" class="block">
    <div class="container text-center">
        <div class="footer-logo">
            <h1 style="font-family: 'Audiowide', cursive; letter-spacing: 5px; text-transform: uppercase">MONIR HOSSAIN</h1>
        </div>
        <ul class="social-icons">
            <li class="wow animated fadeInLeft facebook"><a href="https://goo.gl/KLo5NS" target="_blank"><i class="fa fa-facebook" style="font-size: 20px" aria-hidden="true"></i></a></li>
            <li class="wow animated fadeInLeft instagram"><a href="https://goo.gl/X4bQBb" target="_blank"><i class="fa fa-instagram" style="font-size: 20px" aria-hidden="true"></i></a></li>
            <li class="wow animated fadeInRight twitter"><a href="https://goo.gl/Wffie7" target="_blank"><i class="fa fa-twitter" style="font-size: 20px" aria-hidden="true"></i></a>
            <li class="wow animated fadeInRight gitlab"><a href="https://goo.gl/5kTP2w" target="_blank"><i class="fa fa-gitlab" style="font-size: 20px" aria-hidden="true"></i></a>
            <li class="wow animated fadeInRight twitter"><a href="https://goo.gl/oFi7fq" target="_blank"><i class="fa fa-linkedin" style="font-size: 20px" aria-hidden="true"></i></i></a>
            <li class="wow animated fadeInRight youtube"><a href="https://goo.gl/n7pya3" target="_blank"><i class="fa fa-youtube-play" style="font-size: 20px" aria-hidden="true"></i></a>
        </ul>

        <div class="copyright">
            <div>©2017 Md Monir Hossain, All Rights Reserved</div>
        </div>
    </div><!-- container -->
</section>



<!-- ****************************** Back to top ************************** -->
<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

<?php
include_once '../include/explore/footer.php';
?>