<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Lucy is the best Responsive App Landing Page designed with Bootstrap 3, HTML5 and CSS3. You can use this Responsive  App Landing Page for any kinds of app and game. Amazing design, creative layout and easily customizable.">
    <meta name="keywords" content="Bootstrap, Responsive, App Landing Page">
    <meta name="author" content="ThemeWagon">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <base href="http://www.mhshujon.me/"/>

    <link rel="icon" type="image/png/ico" sizes="16x16" href="assets/welcome/favicon.ico">
    <title>mhshujon</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="assets/exploring/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/exploring/libs/ionicons/css/ionicons.min.css">

    <link rel="stylesheet" href="assets/exploring/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/exploring/css/owl.theme.css">

    <link rel="stylesheet" href="assets/exploring/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" href="assets/exploring/css/nivo-lightbox/nivo-lightbox-theme.css">

<!--    {{--<link rel="stylesheet" href="../../../assets/exploring/css/animate.css">--}}-->
    <link rel="stylesheet" href="assets/exploring/css/style.css">

    <link rel="stylesheet" href="assets/exploring/css/colors/dark.css">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Acme" rel="stylesheet">

    <!-- jquery.fancybox  -->
    <link rel="stylesheet" href="assets/exploring/header/css/jquery.fancybox.css">
    <!-- animate -->
    <link rel="stylesheet" href="assets/exploring/css/hover.min.css">
    <link rel="stylesheet" href="assets/exploring/header/css/animate.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="assets/exploring/header/css/main.css">
    <!-- media-queries -->
    <link rel="stylesheet" href="assets/exploring/header/css/media-queries.css">
    <!-- Modernizer Script for old Browsers -->

    <!-- Modernizr to provide HTML5 support for IE. -->
    <script src="assets/exploring/js/modernizr.custom.js"></script>
    <!-- Modernizer Script for old Browsers -->
    <script src="assets/exploring/header/js/modernizr-2.6.2.min.js"></script>
    <script src="https://use.fontawesome.com/508ba85c2a.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Audiowide&amp;subset=latin-ext" rel="stylesheet">

</head>