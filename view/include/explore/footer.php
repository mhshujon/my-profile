<!-- All the scripts -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="assets/exploring/libs/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/exploring/js/wow.min.js"></script>
    <script src="assets/exploring/js/owl.carousel.js"></script>
    <script src="assets/exploring/js/nivo-lightbox.min.js"></script>
    <script src="assets/exploring/js/smoothscroll.js"></script>
    <script src="assets/exploring/js/jquery.ajaxchimp.min.js"></script>
    <script src="assets/exploring/js/script.js"></script>


    <!-- Essential jQuery Plugins
        ================================================== -->
    <!-- Single Page Nav -->
    <script src="assets/exploring/header/js/jquery.singlePageNav.min.js"></script>
    <!-- jquery.fancybox.pack -->
    <script src="assets/exploring/header/js/jquery.fancybox.pack.js"></script>
    <!-- jquery.mixitup.min -->
    <script src="assets/exploring/header/js/jquery.mixitup.min.js"></script>
    <!-- jquery.parallax -->
    <script src="assets/exploring/header/js/jquery.parallax-1.1.3.js"></script>
    <!-- jquery.countTo -->
    <script src="assets/exploring/header/js/jquery-countTo.js"></script>
    <!-- jquery.appear -->
    <script src="assets/exploring/header/js/jquery.appear.js"></script>
    <!-- jquery easing -->
    <script src="assets/exploring/header/js/jquery.easing.min.js"></script>
    <!-- jquery easing -->
    <script src="assets/exploring/header/js/wow.min.js"></script>
    <script>
        var wow = new WOW({
                boxClass: 'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 120,          // distance to the element when triggering the animation (default is 0)
                mobile: false,       // trigger animations on mobile devices (default is true)
                live: true        // act on asynchronously loaded content (default is true)
            }
        );
        wow.init();
    </script>

    <!-- Custom Functions -->
    <script src="assets/exploring/header/js/custom.js"></script>

</body>
</html>