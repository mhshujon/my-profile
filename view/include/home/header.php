<!DOCTYPE html>
<html class="ie8 oldie" lang="en">
<html lang="en">
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />

    <base href="http://www.mhshujon.me/"/>

    <link rel="icon" type="image/png/ico" sizes="16x16" href="assets/welcome/favicon.ico">
    <title>mhshujon</title>


    <link rel="stylesheet" href="assets/welcome/css/skel.min.css" />
    <link rel="stylesheet" href="assets/welcome/css/style.min.css" />
    <link rel="stylesheet" href="assets/welcome/css/style-wide.min.css" />
    <link rel="stylesheet" href="assets/welcome/css/style-noscript.min.css" />
    <link rel="stylesheet" href="assets/welcome/css/hover.min.css" />

    <link rel="stylesheet" href="assets/welcome/css/ie/v9.css" />
    <link rel="stylesheet" href="/assets/welcome/css/ie/v8.css" />

    <link href="https://fonts.googleapis.com/css?family=Audiowide&amp;subset=latin-ext" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/508ba85c2a.js"></script>

    <script src="assets/welcome/css/ie/html5shiv.js"></script>
    <script src="assets/welcome/js/skel.min.js"></script>
    <script src="assets/welcome/js/init.js"></script>

</head>