<?php
include_once 'vendor/autoload.php';
include_once 'view/include/home/header.php';
?>

<body class="loading">
<div id="wrapper">
    <div id="bg"></div>
    <div id="overlay"></div>
    <div id="main">
        <!-- Header -->
        <header id="header">
            <img src="assets/welcome/image/IMG_6102.png" class="img-responsive"alt="">
            <h2 style="font-family: 'Audiowide', cursive; letter-spacing: 2px; font-size: 60px; text-transform: uppercase">Monir Hossain</h2>
            <p style="font-family: 'Audiowide', cursive; font-size: 13px; text-transform: uppercase">Programmer &nbsp;&bull;&nbsp; Web Developer &nbsp;&bull;&nbsp; Hoping For The Best</p>
            <nav>
                <ul>
                    <li><a href="https://goo.gl/Wffie7" target="_blank" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="https://goo.gl/KLo5NS" target="_blank" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="https://goo.gl/5kTP2w" target="_blank" class="icon fa-gitlab"><span class="label">GitLab</span></a></li>
                    <li><a href="https://goo.gl/p8MrpG" target="_blank" class="icon fa-youtube-play"><span class="label">Youtube</span></a></li>
                    <li><a href="mailto:mhossain143094@bscse.uiu.ac.bd" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                </ul>
            </nav>

            <div style="align-items: center; font-family: 'Audiowide', cursive; font-size: 13px; text-transform: uppercase" class="hvr-grow">
                <a href="view/pages/exploring.php" target="_blank"><h2>Explore More!</h2></a>
            </div>
        </header>
    </div>
</div>

<?php
include_once 'view/include/home/footer.php'
?>